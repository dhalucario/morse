use morse::bot::Bot;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let api_key = std::env::var("TG_TOKEN").expect("No API Key found");
    let mut bot = Bot::new(api_key.to_string(), None).expect("Bot could not be created");
    let (join_handle, mut msg_rx) = bot.start().await;

    while let Some(msg) = msg_rx.recv().await {
        println!("Update:");
        if let Some(chat) = msg.chat {
            if let Err(err) = bot.send_message(chat.id, "Hi".to_string(), None).await {
                println!("Could not send hi: {}", err);
            }
        }
    }

    join_handle.await?;
    Ok(())
}
