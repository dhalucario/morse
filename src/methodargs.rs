use crate::user::User;

enum ParseMode {
    HTML,
    MarkdownV2,
    Markdown
}

struct MessageEntity {
    tg_type: String,
    offset: i32,
    length: i32,
    url: Option<String>,
    user: Option<User>,
    language: Option<String>
}

pub struct SendMessageOptArgs {
    parse_mode: Option<ParseMode>,
    entities: Option<Vec<MessageEntity>>,
    disable_web_page_preview: Option<bool>,
    disable_notification: Option<bool>,
    reply_to_message_id: Option<i32>,
    allow_sending_without_reply: Option<bool>,
    reply_markup: Option<String>
}
