use crate::message::{Message, TelegramResponse};
use crate::methodargs::{SendMessageOptArgs};
use crate::error::MorseHTTPError;
use reqwest::Url;
use reqwest::multipart::Form;

use tokio::sync::{mpsc, Mutex};
use tokio::task::JoinHandle;
use std::sync::{Arc, atomic::AtomicBool, atomic::Ordering};
use std::time::{Duration, SystemTime};

pub struct Bot {
    reqwest_client: Arc<Mutex<reqwest::Client>>,
    api_key: String,
    api_url: String,
    update_url: Url,
    is_polling: Arc<AtomicBool>
}

impl Bot {

    pub fn new(api_key: String, opt_api_origin: Option<String>) -> Result<Bot, Box<dyn std::error::Error>> {
        let reqwest_client = Arc::new(Mutex::new(reqwest::Client::new()));
        let api_origin = opt_api_origin.unwrap_or(String::from("https://api.telegram.org"));
        let api_url = format!("{}/bot{}", api_origin, api_key);
        let update_url: Url = Url::parse(format!("{}{}", api_url, "/getUpdates").as_str())?;

        Ok(Bot {
            reqwest_client,
            api_key,
            api_url,
            update_url,
            is_polling: Arc::new(AtomicBool::new(false))
        })
    }

    pub async fn start(&mut self) -> (JoinHandle<()>, tokio::sync::mpsc::Receiver<Message>) {
        let (tx, mut rx) = mpsc::channel::<Message>(100);

        let is_polling = self.is_polling.clone();
        is_polling.store(true, Ordering::Relaxed);

        let update_url = self.update_url.clone();
        let reqwest_client = self.reqwest_client.clone();
        let reqwest_client = reqwest_client.lock().await;
        let req = reqwest_client.get(update_url.clone());

        let join_handle = tokio::spawn(async move {
            let mut last_poll_time: SystemTime = SystemTime::now();
            let mut last_update_id: i32 = 0;

            while is_polling.load(Ordering::Relaxed) {
                let elapsed_poll_time: Duration = last_poll_time.elapsed().expect("Could not get elapsed time");
                if elapsed_poll_time < Duration::new(2, 0) {
                    let _ = tokio::task::yield_now();
                    continue;
                }

                let this_req = req.try_clone().expect("Could not clone request template.");
                let mut form_data = Form::new();
                form_data = form_data.text("offset", last_update_id.to_string());

                let res = this_req.multipart(form_data).send().await.expect("An error occured while contacting the Telegram servers");
                let res_text = res.text().await.expect("Could not get text from response.");
                let tg_res: TelegramResponse = serde_json::from_str(res_text.as_str()).expect("Could not get text from response.");

                if !tg_res.ok {
                    println!("Response is not okay: {}", res_text);
                }

                for tg_update in tg_res.result {
                    let channel_res = tx.send(tg_update.message).await;
                    if let Ok(_result_sent) = channel_res {
                        if last_update_id < tg_update.update_id {
                            last_update_id = tg_update.update_id+1;
                        }
                    } else if let Err(err) = channel_res {
                        println!("Bot closed: {}", err);
                    }
                }

                last_poll_time = SystemTime::now();
            }
        });

        (join_handle, rx)
    }

    async fn send_form(&mut self, path: &'static str, opt_form_data: Option<Form>) -> Result<(), Box<dyn std::error::Error>> {
        let reqwest_client = self.reqwest_client.clone();
        let reqwest_client = reqwest_client.lock().await;
        let send_message_url = Url::parse(format!("{}{}", self.api_url, path).as_str()).expect("Could not convert to Url.");
        let req = reqwest_client.get(send_message_url);
        let res = req.multipart(opt_form_data.unwrap_or_default()).send().await?;

        if res.status() != 200 {
            return Err(Box::new(MorseHTTPError{
                response_code: res.status().as_u16()
            }));
        }

        println!("{:?}", res);
        Ok(())
    }

    // async fn send_method_json(&mut self, path: &'static str, Option<HashMap<String, String>>) {
    //     let reqwest_client = self.reqwest_client.clone();
    //     let reqwest_client = reqwest_client.lock().await;
    // }

    const SEND_MESSAGE_PATH: &'static str = "/sendMessage";
    pub async fn send_message(&mut self, chat_id: i32, text: String, opt_args: Option<SendMessageOptArgs>) -> Result<(), Box<dyn std::error::Error>> {
        let mut form_data = Form::new();
        form_data = form_data.text("chat_id", chat_id.to_string());
        form_data = form_data.text("text", text);
        self.send_form(Self::SEND_MESSAGE_PATH, Some(form_data)).await?;
        Ok(())
    }


    const GET_ME_PATH: &'static str = "/getMe";
        async fn get_me(&mut self) -> Result<(), Box<dyn std::error::Error>> {
            self.send_form(Self::GET_ME_PATH, None).await?;
            Ok(())
        }
    }
