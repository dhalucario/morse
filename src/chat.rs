use crate::message::Message;
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
pub enum ChatType {
    Private,
    Group,
    SuperGroup,
    Channel
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Chat {
    pub id: i32,
    pub chat_type: Option<ChatType>,
    pub title: Option<String>,
    pub username: Option<String>,
    pub first_name: Option<String>,
    pub last_name: Option<String>,
    // pub photo: ChatPhoto,
    pub bio: Option<String>,
    pub description: Option<String>,
    pub invite_link: Option<String>,
    pub pinned_message: Box<Option<Message>>,
    // pub permissions: Option<ChatPermissions>,
    pub slow_mode_delay: Option<i32>,
    pub message_auto_delete_time: Option<i32>,
    pub sticker_set_name: Option<String>,
    pub can_set_sticker_set: Option<bool>,
    pub linked_chat_id: Option<i32>,
    // pub location: Option<ChatLocation>
}
