use crate::user::User;
use crate::chat::Chat;
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct TelegramResponse {
    pub ok: bool,
    pub result: Vec<TelegramUpdate>
}


#[derive(Serialize, Deserialize, Debug)]
pub struct TelegramUpdate {
    pub update_id: i32,
    pub message: Message
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Message {
    pub message_id: i32,
    pub from: Option<Box<User>>,
    pub sender_chat: Option<Box<Chat>>,
    pub date: Option<i32>,
    pub chat: Option<Box<Chat>>,

    // forward_from: User,
    // forward_from_chat: User,
    // forward_from_message_id: i32,
    // forward_signature: String,
    // forward_sender_name: String,
    // reply_to_message: Message,
    // via_bot: User,
    // edit_date: i32,
    // media_group_id: String,
    // author_signature: String,
    // text: String,
    // entities: Vec<MessageEntities>,
    // animation: Animation,
    // audio: Audio,
    // document: Document,
    // photo: Vec<PhotoSize>,
    // sticker: Sticker,
    // video: Video,
    // video_note: VideoNote,
    // voice: Voice,
    // caption: String,
    // caption_entities: Vec<MessageEntity>,
    // contact: Contract,
    // dice: Dice,
    // game: Game,
    // poll: Poll,
    // venue: Venue,
    // location: Location,
    // new_chat_members: Vec<User>,
    // left_chat_member: User,
    // new_chat_title: String,
    // new_chat_photo: Vec<PhotoSize>,
    // delete_chat_photo: bool,
    // group_chat_created: bool,
    // supergroup_chat_created: bool,
    // channel_chat_created: bool,
    // message_auto_delete_timer_changed: MessageAutoDeleteTimerChanged,
    // migrate_to_chat_id: i32,
    // migrate_from_chat_id: i32,
    // pinned_message: Message,
    // invoice: Invoice,
    // successful_payment: SuccessfulPayment,
    // connected_website: String,
    // passport_data: PassportData,
    // proximity_alert_triggered: ProximityAlertTriggered,
    // voice_chat_scheduled: VoiceChatScheduled,
    // voice_chat_started: VoiceChatStarted,
    // voice_chat_ended: VoiceChatEnded,
    // voice_chat_participants_invited: VoiceChatParticipantsInvited,
    // reply_markup: InlineKeyboardMarkup

}
