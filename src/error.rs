use std::error::Error;
use std::fmt;

#[derive(Debug)]
pub struct MorseHTTPError {
    pub response_code: u16
}

impl fmt::Display for MorseHTTPError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}: ", self.response_code)
    }
}

impl Error for MorseHTTPError {}
